"Vivasvat's vimrc

"--------------------Plugin Stuff--------------------
"-------------------------Pathogen--------------------------
execute pathogen#infect()
filetype plugin indent on
syntax enable
Helptags 

"-------------------------LaTeX Suite--------------------------
let g:tex_flavor='latex'

"-------------------------Ultisnippets Requires--------------------------
let g:UltiSnipsExpandTrigger           = '<tab>'
let g:UltiSnipsJumpForwardTrigger      = '<tab>'
let g:UltiSnipsJumpBackwardTrigger     = '<s-tab>'

"-------------------------Syntastic--------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"SyntasticToggleMode
"SyntasticCheck

"-------------------------MISC--------------------------
set shellslash
set sw=2
set nocompatible
set backspace=indent,eol,start

"-------------------------Colorscheme--------------------------
colorscheme jellybeans
autocmd FileType python colorscheme dracula
autocmd FileType markdown colorscheme apprentice
autocmd FileType tex colorscheme challenger_deep
autocmd FileType c colorscheme jellybeans	
 
"-------------------------Enabling Omni--------------------------
set omnifunc=syntaxcomplete#Complete

"-------------------------Vimwiki--------------------------
let g:vimwiki_list = [{'path': '~/Documents/Vivasvat/Vivasvat/VimNotes/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

"-------------------------jedi_complete for python--------------------------
let g:jedi#auto_initialization = 1

"-------------------------sets number and relative number--------------------------
set relativenumber

if &term == "ansi-term"
  set number
else
  set cursorline
  set number
endif

"-------------------------My Mappings--------------------------
autocmd Filetype c setlocal tabstop=4
